from django.conf.urls.defaults import patterns, url


urlpatterns = patterns("facebookauth.views",
    url(r"^facebook/auth/$", "app_authentication", name="facebook_app_authentication"),
    url(r"^facebook/login/$", "app_authorization", name="facebook_user_authentication"),
)
