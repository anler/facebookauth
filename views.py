import urllib2
import logging
import facebook
from django import http
from django.conf import settings
from django.middleware.csrf import _get_new_csrf_key
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.contrib.sites.models import get_current_site
from models import AccessToken, AccessTokenRequest


logger = logging.getLogger("facebookauth.views.app_authentication")


def app_authentication(request):
    """Ensures user knows what data they are providing.

    GET parameters in any case:
        <csrf> Cross site request forgery token

    GET parameters if allowed:
        <code>

    GET parameters if not allowed:
        <error>
        <error_reason>
        <error_description>
    """
    csrf_token = request.GET.get("csrf")
    try:
        token_request = AccessTokenRequest.live.get(key=csrf_token)
    except AccessTokenRequest.DoesNotExist:
        return http.HttpResponseForbidden('Forbidden')
    try:
        access_code = request.GET["code"]
    except KeyError:
        error_reason = request.GET.get("error_reason", "Unknown reason")
        error = request.GET.get("error", "Unknown error")
        error_description = request.GET.get("error_description")
        redirect_url = reverse(settings.FACEBOOK_APP_AUTHORIZATION_ERROR_REDIRECT)

        logger.error("Facebook App Authorization Failed. Error: %s. Reason: %s. "\
                     "Description: %s" % (error, error_reason, error_description))
    else:
        redirect_uri = get_redirect_uri(request, csrf_token)
        try:
            result = facebook.get_access_token(settings.FACEBOOK_APP_ID,
                                               settings.FACEBOOK_APP_SECRET,
                                               access_code,
                                               redirect_uri)
        except urllib2.URLError, e:
            logger.error("Facebook App Authentication Failed. Error: %s" % e.read())
            raise
        access_token = AccessToken(token=result.get('access_token'),
                                   request=token_request)
        access_token.set_expiration(result.get('expires'))
        access_token.save()
        redirect_url = reverse(settings.FACEBOOK_APP_AUTHENTICATION_REDIRECT)
        redirect_url += '?access_token=' + access_token.token

    return http.HttpResponseRedirect(redirect_url)


def app_authorization(request):
    """Begins the server authentication flow.
    Sets the csrf token."""
    perms = request.GET.get('perms', '').split(",")
    csrf_token = _get_new_csrf_key()
    AccessTokenRequest.live.create(key=csrf_token)
    redirect_uri = get_redirect_uri(request, csrf_token)
    auth_url = facebook.auth_url(settings.FACEBOOK_APP_ID, redirect_uri, perms)
    return http.HttpResponseRedirect(auth_url)


def get_redirect_uri(request, csrf=None, protocol='http'):
    """Returns an uri to the facebookauth's app authentication view.
    This functionality is refactored into its own function because
    apparently Facebook is using the redirect_uri as an internal key to
    encode the code returned for the access_token request.
    see: 
    http://stackoverflow.com/questions/4386691/facebook-error-error-validating-verification-code
    """
    site = get_current_site(request)
    redirect_uri = protocol + '://' + site.domain + reverse(
                    'facebook_app_authentication')
    if csrf:
        redirect_uri += '?csrf=' + csrf
    return redirect_uri
 
