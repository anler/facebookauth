import datetime
from django.db import models

now = datetime.datetime.now

def parse_timedelta(string):
    """Converts strings suchas as "3 days" or "3 seconds" into timedelta
    objects.
    """
    duration, magnitude = string.split()
    expiration_time = {magnitude: int(duration)}
    return datetime.timedelta(**expiration_time)


class LiveTokenManager(models.Manager):
    """Docstring header
    
    Docstring body
    """
    def get_query_set(self):
        return super(LiveTokenManager, self).get_query_set().filter(expires_at__gt=now())


class AccessTokenRequest(models.Model):
    """Docstring header
    
    Docstring body
    """
    key = models.CharField(max_length=40, unique=True)
    expires_at = models.DateTimeField(help_text="Expiration time of the request")

    live = LiveTokenManager()

    def save(self, *args, **kwargs):
        if not self.expires_at:
            expiration_time = kwargs.pop("expiration_time", "10 minutes")
            self.expires_at = now() + parse_timedelta(expiration_time)
        super(AccessTokenRequest, self).save(*args, **kwargs)


class AccessToken(models.Model):
    """Docstring header
    """
    token = models.TextField()
    expires_at = models.DateTimeField(help_text="Expiration time of this token")
    request = models.OneToOneField(AccessTokenRequest, related_name="access_token")

    live = LiveTokenManager()

    def set_expiration(self, expiration):
        """Transforms expiration times suchas as:

            "300" - 300 seconds
            300 - 300 seconds
            "300 seconds" - 300 seconds

        into a datetime object.
        If expiration is already a datetime object, is returned as it is.
        """
        if not isinstance(expiration, datetime.datetime):
            # Facebook returns the expiration time as a string with the number
            # of seconds
            if expiration.isdigit():
                timedelta = parse_timedelta("%s seconds" % expiration)
            else:
                timedelta = parse_timedelta(expiration)
            expiration = now() + timedelta
        self.expires_at = expiration
